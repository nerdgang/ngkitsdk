//
//  Extension.swift
//  NGVideoPlayer
//
//  Created by Havic on 7/11/16.
//  Copyright © 2016 Nerd Gang LLC. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

extension UIColor{
    class func darkness() -> UIColor {
        return .black
    }
}

extension NGPlayerControlsDelegate{
    
    func observePlayerState(_ player:NGPlayer,playerBtnState:UIButton){
        let playerIsPlaying = player.rate > 0
        
        if playerIsPlaying {
            playerBtnState.setImage(#imageLiteral(resourceName: "play"), for: UIControlState.normal)
            player.pause()
        }else{
            playerBtnState.setImage(#imageLiteral(resourceName: "pause_filled"), for: UIControlState.normal)
            player.play()
        }
    }
   
}

extension NGPlayerViewController{
    
    func updateTimeLabel(_ elapsedTime: Float64, duration: Float64) {
        let timeRemaining: Float64 = CMTimeGetSeconds((ngplayer.currentItem?.duration)!) - elapsedTime
        timeRemainingLabel.text = String(format: "%02d:%02d", ((lround(timeRemaining) / 60) % 60), lround(timeRemaining) % 60)
    }
    
    func observeTime(_ elapsedTime: CMTime) {
        let duration = CMTimeGetSeconds((ngplayer.currentItem?.duration)!)
        if __inline_isfinitef(Float(duration)) > 0 {
            let elapsedTime = CMTimeGetSeconds(elapsedTime)
            updateTimeLabel(elapsedTime, duration: duration)
        }
    }
    
    func sliderBeganTracking(_ slider: UISlider) {
        ngplayerRateBeforeSeek = ngplayer.rate
        ngplayer.pause()
    }
    
    func sliderEndedTracking(_ slider: UISlider) {
        let videoDuration = CMTimeGetSeconds(ngplayer.currentItem!.duration)
        let elapsedTime: Float64 = videoDuration * Float64(seekSlider.value)
        updateTimeLabel(elapsedTime, duration: videoDuration)
        
        ngplayer.seek(to: CMTimeMakeWithSeconds(elapsedTime, 100)) { (completed: Bool) -> Void in
            if self.ngplayerRateBeforeSeek > 0 {
                self.ngplayer.play()
            }
        }
    }
    
    func sliderValueChanged(_ slider: UISlider) {
        let videoDuration = CMTimeGetSeconds(ngplayer.currentItem!.duration)
        let elapsedTime: Float64 = videoDuration * Float64(seekSlider.value)
        updateTimeLabel(elapsedTime, duration: videoDuration)
    }
    
    func trackMediaProgress(_ ngplayerProgress:CMTime){
        let playerProgressTime = Float(CMTimeGetSeconds(ngplayerProgress))
        let videoDuration = Float(CMTimeGetSeconds(ngplayer.currentItem!.duration))
        seekSlider.minimumValue = 0
        seekSlider.maximumValue = videoDuration
        seekSlider.setValue(0.01, animated: true)
        seekSlider.value = playerProgressTime
    }
}

extension String{
    
    var lastPathComponent: String {
        
        get {
            return (self as NSString).lastPathComponent
        }
    }
    var pathExtension: String {
        
        get {
            
            return (self as NSString).pathExtension
        }
    }
    var stringByDeletingLastPathComponent: String {
        
        get {
            
            return (self as NSString).deletingLastPathComponent
        }
    }
    var stringByDeletingPathExtension: String {
        
        get {
            
            return (self as NSString).deletingPathExtension
        }
    }
    var pathComponents: [String] {
        
        get {
            
            return (self as NSString).pathComponents
        }
    }
    
    public func stringByAppendingPathComponent(_ path: String) -> String {
        
        let nsSt = self as NSString
        
        return nsSt.appendingPathComponent(path)
    }
    
    func stringByAppendingPathExtension(_ ext: String) -> String? {
        
        let nsSt = self as NSString
        
        return nsSt.appendingPathExtension(ext)
    }
}



