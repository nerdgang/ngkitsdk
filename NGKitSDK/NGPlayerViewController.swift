//
//  NGPlayerViewController.swift
//  NGKit
//
//  Created by Havic on 8/24/16.
//  Copyright © 2016 Nerd Gang Inc. All rights reserved.
//

import UIKit
import AVFoundation

class NGPlayerViewController: UIViewController,NGPlayerControlsDelegate {
    var ngplayer = NGPlayer()
    var ngPlayerLayer: NGPlayerLayer!
    var timeObserver: Any!
    var ngPlayerControls: NGPlayerControlsDelegate?
    var ngplayerRateBeforeSeek: Float = 0
    var ngplayerProgressTime = Float()
    fileprivate var playbackLikelyToKeepUpContext = 0
    
    @IBOutlet weak var playerVIew: UIView!
    @IBOutlet weak var playerBtnState: UIButton!
    @IBOutlet weak var timeRemainingLabel: UILabel!
    @IBOutlet weak var seekSlider: UISlider!
    @IBOutlet weak var loadingIndicatorView: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view.backgroundColor? = .darkness()
        timeRemainingLabel.text = ""
        loadingIndicatorView.hidesWhenStopped = true
        ngPlayerLayer = NGPlayerLayer(player: ngplayer)
        playerVIew.layer.insertSublayer(ngPlayerLayer, at: 0)
        view.insertSubview(playerVIew, at: 0)
        ngplayer.addObserver(self, forKeyPath: "currentItem.playbackLikelyToKeepUp", options: .new, context: &playbackLikelyToKeepUpContext)
        
        guard let url = URL(string: "https://content.jwplatform.com/manifests/vM7nH0Kl.m3u8") else{return}
        
        let playerItem = AVPlayerItem(url: url)
        ngplayer.replaceCurrentItem(with: playerItem)
        
        timeRemainingLabel.textColor = UIColor.white
        
        seekSlider.addTarget(self, action: #selector(sliderBeganTracking),
                             for: .touchDown)
        seekSlider.addTarget(self, action: #selector(sliderEndedTracking),
                             for: [.touchUpInside, .touchUpOutside])
        seekSlider.addTarget(self, action: #selector(sliderValueChanged),
                             for: .valueChanged)
        
        let timeInterval: CMTime = CMTimeMakeWithSeconds(1.0, 10)
        timeObserver = ngplayer.addPeriodicTimeObserver(forInterval: timeInterval,
                                                        queue: DispatchQueue.main) { (elapsedTime: CMTime) -> Void in
                                                            
                                                            print("elapsedTime now:", CMTimeGetSeconds(elapsedTime))
                                                            self.observeTime(elapsedTime)
                                                            self.trackMediaProgress(elapsedTime)
        }
    }
    
    deinit {
        ngplayer.removeTimeObserver(timeObserver)
        ngplayer.removeObserver(self, forKeyPath: "currentItem.playbackLikelyToKeepUp")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadingIndicatorView.startAnimating()
        ngplayer.play()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        // Layout subviews manually
        ngPlayerLayer.frame = playerVIew.bounds
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func addObserver(_ observer: NSObject, forKeyPath keyPath: String, options: NSKeyValueObservingOptions = [], context: UnsafeMutableRawPointer?) {
        if context == &playbackLikelyToKeepUpContext {
            if ngplayer.currentItem!.isPlaybackLikelyToKeepUp {
                loadingIndicatorView.stopAnimating()
            } else {
                loadingIndicatorView.startAnimating()
            }
        }
    }
    
    
    @IBAction func playerButtonTapped(_ sender: UIButton) {
        self.observePlayerState(ngplayer, playerBtnState: playerBtnState)
    }

}
