//
//  HelperClass.swift
//  NGVideoPlayer
//
//  Created by Havic on 7/11/16.
//  Copyright © 2016 Nerd Gang LLC. All rights reserved.
//

import Foundation
import AVFoundation

public typealias NGPlayer = AVPlayer
typealias NGPlayerLayer = AVPlayerLayer
typealias NGPlayerItem = AVPlayerItem

@objc protocol NGPlayerControlsDelegate {
    @objc optional func observePlayerState()
}
