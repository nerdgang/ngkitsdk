//
//  NGPageVIewController.swift
//  NerdKit
//
//  Created by Havic on 12/6/16.
//  Copyright © 2016 Nerd Gang Inc. All rights reserved.
//

import Foundation
import UIKit

class NGPageVIewController: UIPageViewController,UIPageViewControllerDelegate,UIPageViewControllerDataSource {
    lazy var vcArray: [UIViewController] = {
        return [self.vcInstance(name: "CIHOnboardingVCLeft"), self.vcInstance(name: "CIHOnboardingVCMiddle"),self.vcInstance(name: "CIHOnboardingVCRight")]
        
    }()
    
    private func vcInstance(name:String)->UIViewController{
        
        return UIStoryboard(name: "OnboardingStoryboard", bundle: nil).instantiateViewController(withIdentifier: name)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        delegate = self
        dataSource = self
        guard let firstVC = vcArray.first else {
            return
        }
        setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        for view in self.view.subviews{
            if view is UIScrollView {
                view.frame = UIScreen.main.bounds
            }else if view is UIPageControl{
                view.backgroundColor = .clear
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = vcArray.index(of: viewController) else { return nil }
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard vcArray.count > previousIndex else {
            return nil
        }
        
        return vcArray[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = vcArray.index(of: viewController) else { return nil }
        let nextIndex = viewControllerIndex + 1
        
        guard nextIndex < vcArray.count else {
            return nil
        }
        
        guard vcArray.count > nextIndex else {
            return nil
        }
        
        return vcArray[nextIndex]
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int{
        
        // The number of items reflected in the page indicator.
        return vcArray.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        
        // The selected item reflected in the page indicator.
        guard let firstViewController = viewControllers?.first, let firstViewControllerIndex = vcArray.index(of: firstViewController) else { return 0 }
        return firstViewControllerIndex
    }
}
